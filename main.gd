@tool
extends Node2D

## LATEST INFO (FEB 7th 2024)
##
## Changing a resource *in inspector*, then ctrl + S always saves.
## Saving from the inspector icons also works as expected.
##
## Saving from code otoh:
## 4.3.dev
## Press : Loadres (once) (creates it if it's not there)
## (Now open thing.tres in an external editor so you can watch it)
## Press : Change Array Only, Ctrl+s == it saves ONLY the first time
## Press : Change Array Only, Ctrl+s == it does NOT save again
##
## Press : Change All, Ctrl+s == it saves EVERY TIME
##
## ∴ Something about arrays in resources is wonky. It may be related
## to https://github.com/godotengine/godot/issues/59669 - not sure.
##
## No regression, per say, could be found. Same happens on:
## Godot_v4.2-stable_linux.x86_64
## Godot_v4.2-rc1_linux.x86_64
## Godot_v4.2.1-stable_linux.x86_64
## Godot_v4.2.1-rc1_linux.x86_64
## Godot_v4.1.1-stable_linux.x86_64

# Full output - You can see when data AND name change, then ctrl+s saves.
# But if "Change Array Only" is pressed, it doesn't save (after the first time)

#CREATE
#LOAD
#thing name:Name0
#thing data:[1]
#^Z
#$ cat thing.tres
#[gd_resource type="Resource" script_class="Thing" load_steps=2 format=3 uid="uid://cf80e3igckkne"]
#[ext_resource type="Script" path="res://thing.gd" id="1_g34hl"]
#[resource]
#script = ExtResource("1_g34hl")
#data = Array[int]([1])
#name = "Name0"
#$ fg

#CHANGE <-- first press of Change Array Only
#thing data changed to:[1, 2] (I press ctrl+S here)
#^Z
#$ cat thing.tres
#[gd_resource type="Resource" script_class="Thing" load_steps=2 format=3 uid="uid://chor4rj0lgott"]
#[ext_resource type="Script" path="res://thing.gd" id="1_g34hl"]
#[resource]
#script = ExtResource("1_g34hl")
#data = Array[int]([1, 2])
#name = "Name0"
#$ fg

#CHANGE <-- Second press of Change Array Only
#thing data changed to:[1, 2, 3] (I press ctrl+S here)
#^Z
#$ cat thing.tres
#[gd_resource type="Resource" script_class="Thing" load_steps=2 format=3 uid="uid://chor4rj0lgott"]
#[ext_resource type="Script" path="res://thing.gd" id="1_g34hl"]
#[resource]
#script = ExtResource("1_g34hl")
#data = Array[int]([1, 2])  <-- the array is wrong
#name = "Name0"

#$ fg
#CHANGE <-- Chnage All was pressed
#thing name changed to:Name3
#thing data changed to:[1, 2, 3, 4] (I press ctrl+S here)
#^Z
#$ cat thing.tres
#[gd_resource type="Resource" script_class="Thing" load_steps=2 format=3 uid="uid://chor4rj0lgott"]
#[ext_resource type="Script" path="res://thing.gd" id="1_g34hl"]
#[resource]
#script = ExtResource("1_g34hl")
#data = Array[int]([1, 2, 3, 4]) <-- The array has updated
#name = "Name3"

#$ fg
#CHANGE <-- I do more tests
#thing data changed to:[1, 2, 3, 4, 5] (I press ctrl+S here)
#CHANGE
#thing data changed to:[1, 2, 3, 4, 5, 6] (I press ctrl+S here)
#^Z
#$ cat thing.tres
#[gd_resource type="Resource" script_class="Thing" load_steps=2 format=3 uid="uid://chor4rj0lgott"]
#[ext_resource type="Script" path="res://thing.gd" id="1_g34hl"]
#[resource]
#script = ExtResource("1_g34hl")
#data = Array[int]([1, 2, 3, 4])
#name = "Name3"
#donn@ddm ~/resource_save_ext_issue

#$ fg
#CHANGE
#thing name changed to:Name6
#thing data changed to:[1, 2, 3, 4, 5, 6, 7] (I press ctrl+S here)
#^Z
#$ cat thing.tres
#[gd_resource type="Resource" script_class="Thing" load_steps=2 format=3 uid="uid://chor4rj0lgott"]
#[ext_resource type="Script" path="res://thing.gd" id="1_g34hl"]
#[resource]
#script = ExtResource("1_g34hl")
#data = Array[int]([1, 2, 3, 4, 5, 6, 7])
#name = "Name6"


@export var loadres:bool:
	set(t):
		loadres=false
		_load()

@export var change_array_only:bool:
	set(t):
		change_array_only=false
		_change(1)

@export var change_all:bool:
	set(t):
		change_all=false
		_change(2)

var thing:Thing
func _load() -> void:
	thing = load("res://thing.tres")
	if not thing:
		print("CREATE")
		thing = Thing.new()
		thing.data.append(1)
		thing.name = "Name0"
		ResourceSaver.save(thing,"thing.tres")

	# I had to call load again here in order to get this
	# siutation to be reproducible. Without this,
	# the resource *never* gets written to by ctrl+S
	thing = load("res://thing.tres")

	print("LOAD")
	print("thing name:", thing.name)
	print("thing data:", thing.data)

func _change(wot):
	var n = thing.data[-1]
	thing.data.append(n+1)
	if wot==2:
		thing.name = "Name%s" % n
	print("CHANGE")
	if wot==2:
		print("thing name changed to:", thing.name)
	print("thing data changed to:", thing.data, " (I press ctrl+S here)")
