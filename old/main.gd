@tool
extends Node2D

## INFO
##
## Resource "db.tres" (class db.gd) holds ref to "thing.tres" (thing.gd)
## which has Array[String].
## Changing that array and saving* "db.tres" does not save "thing.tres"
## (Only noticed this on 4.3.dev)
##
## *Saving, means:
## 1. ResourceSaver.save and
## 2. Ctrl+S in editor.
##
## ResourceSaver.save saves "db.tres" but does _not_ save "thing.tres"
## (I am not sure if it was always thus. The docs specify recursive saving.)
##
## Ctrl+S only *sometimes* saves "thing.tres" when it changes.
## NOT ALWAYS. I can even exit Godot and have thing.tres no be updated.
##
## If you press Ctrl+S while thing.tres is open in the inspector it seems
## to save more relialbly.
## If you press Ctrl+S when the cursor is in the code, or on the top menu
## somewhere, there is more chance the tres will _not_ be saved.
##
## Demo
##
## Toggle loadres to start.
## Add A and B will append to the array
## Do Save will save db.tres
## Open thing.tres externally and observe the actual contents.
##
## v4.3.dev.custom_build [1c8ae43ef]
## Godot v4.3.dev (1c8ae43ef) - Ubuntu 22.04.3 LTS 22.04 - X11 - Vulkan (Forward+) - dedicated NVIDIA GeForce RTX 2060 (nvidia; 535.154.05) - Intel(R) Core(TM) i5-9400F CPU @ 2.90GHz (6 Threads)

@export var loadres:bool:
	set(t):
		loadres=false
		_load()

@export var add_A:bool:
	set(t):
		add_A=false
		_change("ADDED A")

@export var add_B:bool:
	set(t):
		add_B=false
		_change("ADDED B")

@export var do_save:bool:
	set(t):
		do_save=false
		_save()

var thing:Thing
func _load() -> void:
	thing = Thing.new()
	ResourceSaver.save(thing,"thing.tres")
	thing = load("res://thing.tres")
	print("thing loaded")

func _change(txt):
	thing.myname.append(txt)
	print("thing myname now:", thing.myname)

func _save():
	if not thing:
		print("not loaded yet")
		return
	var db = DB.new()
	db.data = thing
	print("Saving db. Contains:", thing.myname)

	# I have become accustomed to this step also
	# saving all (changed) ExtResources contained within.
	# It seems not to be doing this any more.
	ResourceSaver.save(db,"db.tres")

